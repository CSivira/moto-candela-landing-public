const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
const mongoURL = 'mongodb+srv://cesarisimo:aix2UK2JiFTOz1cl@cluster0.tdgarid.mongodb.net/moto-candela'
//const mongoURL = 'mongodb+srv://casmsvr:JmrGFTNVako4JMn6@cluster0.tdgarid.mongodb.net/'

app.use(cors());

mongoose.connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

const MotorcycleSchema = new mongoose.Schema({
    name: String,
    id: Number,
    category: String,
    image: String,
});

const ReplacementSchema = new mongoose.Schema({
    name: String,
    id: Number,
    code: String,
    image: String,
});

const CategorySchema = new mongoose.Schema({
    name: String,
    id: String,
});

const Motorcycle = mongoose.model("Motorcycle", MotorcycleSchema);
const Replacement = mongoose.model("Replacement", ReplacementSchema);
const Category = mongoose.model("Category", CategorySchema);

app.get("/api/rest/v1/motorcycles/:id", async (req, res) => {
    const motorcycle = await Motorcycle.findOne({ id: req.params.id });
    res.json(motorcycle);
});

app.get("/api/rest/v1/motorcycles", async (req, res) => {
    const motorcycles = await Motorcycle.find();
    res.json(motorcycles);
});

app.get("/api/rest/v1/replacements/:id", async (req, res) => {
    const replacement = await Replacement.findOne({ id: req.params.id });
    res.json(replacement);
});

app.get("/api/rest/v1/replacements", async (req, res) => {
    const replacements = await Replacement.find();
    res.json(replacements);
});

app.get("/api/rest/v1/categories", async (req, res) => {
    const categories = await Category.find();
    res.json(categories);
});

app.listen(4000, () => {
    console.log("Server is running on port 4000");
});
